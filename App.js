/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {
  ActivityIndicator,
  Animated,
  Easing,
  FlatList,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {Component} from 'react';

type State = {
  loading: boolean,
  button: any,
  bars: any,
  limit: number,
  showPicker: boolean,
  selectedBar?: any,
};

class App extends Component<State> {
  anim = new Animated.Value(0);

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      bars: [],
      buttons: [],
      limit: 0,
      showPicker: false,
    };
  }

  componentDidMount() {
    this.callEndpoint();
  }

  onAnimate = (value, index) => {
    if (this.state.progressBar && this.state.progressBar[index]) {
      Animated.timing(this.state.progressBar[index], {
        toValue: value,
        duration: 500,
        easing: Easing.linear,
      }).start();
    }
  };

  callEndpoint = () => {
    let selection = [];
    let progressBar = [];

    fetch('http://pb-api.herokuapp.com/bars')
      .then(res => res.json())
      .then(response => {
        console.log(response);

        response.bars.map((item, index) => {
          selection.push({id: index, name: `#progress${index + 1}`});
          progressBar.push(new Animated.Value(0));
        });

        this.setState({
          selection,
          progressBar,
          selectedBar: selection[0],
          buttons: response.buttons,
          bars: response.bars,
          limit: response.limit,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({loading: false});
        console.error(error);
      });
  };

  onPressSelection = visible => () => {
    this.setState({showPicker: visible});
  };

  renderButton = ({item, index}) => {
    const {selectedBar, bars, limit} = this.state;

    return (
      <TouchableOpacity
        disabled={!selectedBar}
        style={[
          styles.pickerButton,
          {backgroundColor: !selectedBar ? '#DDD' : '#87CEEB'},
        ]}
        onPress={() => {
          let percentage =
            bars[selectedBar.id] > 0
              ? ((bars[selectedBar.id] / limit) * 100).toFixed(0)
              : 0;

          let newPercentage = parseInt(percentage) + item;

          if (newPercentage <= 0) {
            bars[selectedBar.id] = 0;
            newPercentage = 0;
          } else {
            let count = ((newPercentage / 100) * limit).toFixed(0);
            bars[selectedBar.id] = parseInt(count);

            if (percentage > 100) {
              newPercentage = 100;
            }
          }

          console.log('newPercentage' + index, newPercentage);

          this.onAnimate(newPercentage, index);

          this.setState({bars});
        }}>
        <Text style={styles.buttonText}>{item}</Text>
      </TouchableOpacity>
    );
  };

  renderBar = ({item, index}) => {
    const {limit} = this.state;

    let percentage = (item / limit) * 100;
    let newProgress = percentage;

    if (percentage > 100) {
      newProgress = 100;
    } else if (percentage < 0) {
      newProgress = 0;
      percentage = 0;
    }

    this.onAnimate(newProgress, index);

    return (
      <View style={styles.progressBarView}>
        <Animated.View
          style={[
            styles.progressBar,
            {
              backgroundColor: percentage > 100 ? '#FF0000' : '#87CEEB',
              width: this.state.progressBar
                ? this.state.progressBar[index].interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0%', '1%'],
                  })
                : 0,
            },
          ]}
        />
        <Text style={styles.percentageText}>{`${percentage.toFixed(0)}%`}</Text>
      </View>
    );
  };

  render() {
    const {
      bars,
      buttons,
      showPicker,
      selection,
      selectedBar,
      loading,
    } = this.state;

    return (
      <View style={styles.containerView}>
        <Text style={styles.headerText}>Progress Bar Demo</Text>
        <Text style={styles.noteText}>by Shin Wey Tan</Text>

        {loading ? (
          <ActivityIndicator size="large" />
        ) : (
          <FlatList
            scrollEnabled={false}
            data={bars}
            renderItem={this.renderBar}
            keyExtractor={(item, index) => index.toString()}
            ListFooterComponent={
              <View style={styles.footerView}>
                {bars?.length > 0 ? (
                  <TouchableOpacity
                    style={styles.selectButton}
                    onPress={this.onPressSelection(true)}>
                    <Text style={styles.buttonText}>
                      {selectedBar ? selectedBar.name : 'Select'}
                    </Text>
                  </TouchableOpacity>
                ) : null}
                <FlatList
                  horizontal
                  data={buttons}
                  renderItem={this.renderButton}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            }
          />
        )}

        <Modal
          visible={showPicker}
          animationType={'slide'}
          transparent={true}
          onRequestClose={this.onPressSelection(false)}>
          <View style={styles.modalView}>
            <View style={styles.modalHeaderView}>
              <Text style={styles.modalTitle}>Select progress bar</Text>
              <TouchableOpacity onPress={this.onPressSelection(false)}>
                <Text style={styles.modalCancelButton}>Cancel</Text>
              </TouchableOpacity>
            </View>
            {selection?.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  onPress={() =>
                    this.setState({selectedBar: item, showPicker: false})
                  }
                  style={styles.modalOptionButton}>
                  <Text>{item.name}</Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {
    flex: 1,
    padding: 30,
  },
  pickerButton: {
    width: 50,
    paddingVertical: 10,
    marginRight: 10,
    borderRadius: 10,
    alignItems: 'center',
  },
  selectButton: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: '#DDD',
    marginBottom: 10,
    borderRadius: 15,
  },
  headerText: {
    fontSize: 32,
    textAlign: 'center',
    fontWeight: 'bold',
    marginTop: '10%',
  },
  noteText: {
    marginTop: 10,
    marginBottom: '10%',
    fontSize: 18,
    textAlign: 'center',
  },
  buttonText: {
    fontSize: 16,
  },
  percentageText: {
    fontSize: 18,
  },
  footerView: {
    // flexDirection: 'row',
    marginTop: 20,
  },
  modalView: {
    margin: 10,
    padding: 20,
    backgroundColor: '#EEE',
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
    borderRadius: 20,
  },
  modalHeaderView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 10,
  },
  modalTitle: {
    fontWeight: 'bold',
  },
  modalCancelButton: {
    color: '#999',
    fontWeight: 'bold',
  },
  modalOptionButton: {
    paddingVertical: 5,
  },
  progressBarView: {
    borderWidth: 1,
    borderColor: '#999',
    flexDirection: 'row',
    flex: 1,
    height: 50,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  progressBar: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
  },
});

export default App;
